This is a simple perl script that uses the gitlab API to export all the issues in a project in CSV format.

CSV export is built in to gitlab EE:

https://docs.gitlab.com/ee/user/project/issues/csv_export.html

However depending on the version it may not include all fields, and as the export is emailed to you gitlab limit the size to 20MB.

Hence this script is mainly of use to gitlab CE users or users that want to export large number of issues.

It uses the CSV_XS perl module - on Debian based systems you would install this
using something like:

`sudo apt-get install libtext-csv-perl`

on other systems, it may be easiest to install using CPAN.

## Usage

Move to a path where `get-all-project-issues.pl` exists, then:

```
./get-all-project-issues.pl %PROJECTID% %YOUR-TOKEN%
./get-all-project-issues.pl 12345678 Your-ToKenXXXXXXXXX
```
Result will be saved on the current path with a file name `issues_ProjectID-$PROJECTID.csv` (`$PROJECTID` to be replaced with what you pass).

EoF